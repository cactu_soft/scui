import components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

export default function createComponents(viteEnv) {
    const { VITE_APP_ENV } = viteEnv
    return components({
        resolvers: VITE_APP_ENV === 'production'
            ? ElementPlusResolver()
            : undefined,
        // allow auto load markdown components under `./src/components/`
        extensions: ['vue'],
        // allow auto import and register components used in markdown
        include: [/\.vue$/, /\.vue\?vue/],
        dts: false
    })
}
