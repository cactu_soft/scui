
<div align="center">

<img src="public/img/logo.png" width="50" alt="LOGO">

<p align="center">
	<a href="https://v3.vuejs.org/" target="_blank">
		<img src="https://img.shields.io/badge/VueCLI-5-green" alt="VueCLI">
	</a>
	<a href="https://v3.vuejs.org/" target="_blank">
		<img src="https://img.shields.io/badge/Vue.js-3.x-green" alt="Vue">
	</a>
	<a href="https://element-plus.gitee.io/#/zh-CN/component/changelog" target="_blank">
		<img src="https://img.shields.io/badge/element--plus-latest-blue" alt="element plus">
	</a>
</p>

<h1>SCUI Admin</h1>

</div>

## 介绍
SCUI 是一个中后台前端解决方案，基于VUE3和elementPlus实现。
使用最新的前端技术栈，提供各类实用的组件方便在业务开发时的调用，并且持续性的提供丰富的业务模板帮助你快速搭建企业级中后台前端任务。

SCUI的宗旨是 让一切复杂的东西傻瓜化。

## 关于Vite版本的说明

### 为啥要搞vite版本

1. 好奇心：一位充满好奇心的Java程序员在接触到SCUI后，立刻被其深深吸引，爱不释手。
2. 探索欲：在这个过程中，了解vite后，又产生了vite版本的想法。
3. 技术力：弱（java也弱），但是通过不断的搜索（借鉴）与专研（相关技术也看到后半夜），终于弄出来一版。

> 一直想寻转一款好用的中后台框架，到现在（24年5月24日）为止应该是有10多天了，用了市面上的很多款，最终选择了scui。
> 
> 我必须要吹捧scui，我的前端技术力弱，但是看scui就好像能看明白，市面上提前的就看不懂，或者说用起来就繁琐。
> 
> 找到scui后爱不释手，天天捣鼓，也在生成中用到了，但是开发过程中不得不说和vite比，慢，又开始不满足了。
> 
> 有开始找vite版本，说实话都有那么一丢丢瑕疵（哈哈），其实从业务角度说足够用了，单纯的耐不住好奇、探索。
> 
> 在吹一下原作者，真的是很棒，很多模板直接就可以用。

### 下面说点大家关心的
1. 状态管理有pinia，但是也保留了，vuex；在过程中会慢慢改变成纯pinia。
2. 升级element-plus到2.7.3，部分页面没有适配，但是不影响整体。

## 演示和文档 - 后续更新一下吧

| 类型 | 链接-原作品暂时不可用                                     |
| -------- |-------------------------------------------------|
| 文档地址 | https://lolicode.gitee.io/scui-doc/             |
| 演示地址  | https://lolicode.gitee.io/scui-doc/demo/#/login |



## 特点

- **组件** 多个独家组件、业务模板
- **权限** 完整的鉴权体系和高精度权限控制
- **布局** 提供多套布局模式，满足各种视觉需求
- **API** 完善的API管理，使用真实网络MOCK
- **配置** 统一的全局配置和组件配置，支持build后配置热更新
- **性能** 在减少带宽请求和前端算力上多次优化，并且持续着
- **其他** 多功能视图标签、动态权限菜单、控制台组态化、统一异常处理等等


## 部分截图 - 后续在贴


## 安装教程
``` sh
# 克隆项目
git clone https://gitee.com/qiseliu/scui.git

# 进入项目目录
cd scui

# 安装依赖
npm i

# 启动项目(开发模式)
npm run serve
```
启动完成后浏览器访问 http://localhost:2800

## 鸣谢-新

[SCUI](https://gitee.com/lolicode/scui)
[SCUI-VITE](https://gitee.com/asddfff/scui-vite)

## 支持
如果觉得本项目还不错或在工作中有所启发，请在Gitee(码云)帮开发者点亮星星，这是对开发者最大的支持和鼓励！
