import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import path from 'path'


import createVitePlugins from "./vite/plugins";


// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {

  const env = loadEnv(mode, process.cwd())
  const { VITE_APP_ENV } = env
  return {
    base: VITE_APP_ENV === 'production' ? env.VITE_BASE_URL : env.VITE_BASE_URL,
    plugins: createVitePlugins(env, command === 'build'),
    resolve: {
      alias: {
        // 设置路径
        '~': path.resolve(__dirname, './'),
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
      },
      extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
    },
    build:{
      outDir:env.VITE_BUILD_OUTDIR,
    },
    server: {
      host: '0.0.0.0',
      port: env.VITE_APP_PORT,
      proxy: {
          "api": {
            target: env.VITE_APP_API_BASEURL,
            changeOrigin: true,
            ws: true,
            rewrite: (path) => path.replace(/^\/api/, ""),
          }
      }
    },
    css: {
      postcss: {
        plugins: [
          {
            postcssPlugin: 'internal:charset-removal',
            AtRule: {
              charset: (atRule) => {
                if (atRule.name === 'charset') {
                  atRule.remove();
                }
              }
            }
          }
        ]
      }
    }
  }
})
