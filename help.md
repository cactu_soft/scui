## 插件的作用
1. codemirror：代码编辑器
2. cropperjs：图片裁切
3. crypto-js：对称加密
4. nprogress： NProgress一般与axios相结合,实时监听axios请求的进度,实现实时反馈进度的效果,增强用户体验。
5. vuedraggable：拖拽功能
6. xgplayer：播放器功能
7. unplugin-auto-import：自动导入功能。